package controllers

import org.scalatest.concurrent.ScalaFutures
import org.scalatestplus.play._
import org.scalatestplus.play.guice.GuiceOneAppPerTest
import play.api.test.Helpers._
import play.api.test._

class HomeControllerSpec extends PlaySpec with GuiceOneAppPerTest with ScalaFutures {
  "HomeController GET" should {

    "render the index page from a new instance of controller" in {
      val request = FakeRequest(GET, "/")
      route(app, request) match {
        case Some(future) =>
          whenReady(future) { result =>
            result.header.status mustEqual OK
            result.body.contentType mustBe Some("text/html; charset=utf-8")
          }
        case None => fail
      }
    }

  }
}
