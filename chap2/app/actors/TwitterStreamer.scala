package actors

import akka.Done
import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.stream.Materializer
import akka.stream.scaladsl.Sink
import akka.util.ByteString
import play.api.Logger
import play.api.libs.json.{JsValue, Json}
import play.api.libs.ws.{WSClient, WSResponse, WSSignatureCalculator}

import scala.concurrent.{ExecutionContext, Future}

class TwitterStreamer(out: ActorRef, ws: WSClient, calc: WSSignatureCalculator)
                     (implicit ec: ExecutionContext, mat: Materializer) extends Actor with ActorLogging {
  override def receive: Receive = {
    case "subscribe" =>
      Logger.info("Received subscription from a client")
      TwitterStreamer.subscribe(out, ws, calc)
  }

  override def postStop(): Unit = {
    Logger.info("Unsubscribe client from stream")
    TwitterStreamer.unsubscribe(out)
  }
}

object TwitterStreamer {
  private var isInitialized = false
  private var unusedString: String = ""
  private val subscribers = scala.collection.mutable.Set[ActorRef]()
  private val replicators = scala.collection.mutable.Set[ActorRef]()

  def props(out: ActorRef, ws: WSClient, calc: WSSignatureCalculator)
           (implicit ec: ExecutionContext, mat: Materializer)= Props(new TwitterStreamer(out, ws, calc))

  private def init(ws: WSClient, calc: WSSignatureCalculator)
                  (implicit ec: ExecutionContext, mat: Materializer) : Unit = {
    if (!isInitialized) {
      isInitialized = true
      startStreaming(ws, calc)
    }
  }

  def subscribe(out: ActorRef, ws: WSClient, calc: WSSignatureCalculator)
               (implicit ec: ExecutionContext, mat: Materializer): Unit = {
    subscribers += out
    init(ws, calc)
  }

  def replicate(out: ActorRef): Unit = {
    replicators += out
  }

  def unsubscribe(out: ActorRef): Unit = {
    subscribers -= out
  }

  private def startStreaming(ws: WSClient, calc: WSSignatureCalculator)
                            (implicit ec: ExecutionContext, mat: Materializer): Unit = {
    val mayBeMasterNodeUrl = Option(System.getProperty("masterNodeUrl"))
    val url = mayBeMasterNodeUrl.getOrElse("https://stream.twitter.com/1.1/statuses/filter.json")
    val twitterStream: Future[WSResponse] = ws.url(url)
      .sign(calc)
      .withQueryStringParameters("track" -> "cat")
      .withMethod("GET")
      .stream()

    // The sink that writes to the output stream
    // handle partial tweets
    val sink: Sink[ByteString, Future[Done]] = Sink.foreach[ByteString] { bytes =>
      replicators.foreach(_ ! bytes)
      val tweets: Array[String] = bytes.utf8String.split("\\r\\n")
      val last: String = if (tweets.nonEmpty) tweets.last else new String
      unusedString += last
      if (("""\"timestamp_ms\":\"\d+\"}\Z""".r  findAllIn  unusedString).nonEmpty) {
        try {
          val value: JsValue = Json.parse(unusedString)
          val user: JsValue = value("user")
          Logger.info(user("name").toString())
          subscribers.foreach(_ ! value.toString())
        } catch {
          case e: Exception => Logger.error("Exception caught:", e)
        } finally {
          unusedString = ""
        }
      }

      if (tweets.length > 1) {
        Logger.warn("TODO: handle more than one tweet in a response")
      }
    }

    val streamToFuture: Future[Done] = twitterStream.flatMap {
      response =>
        val source = response.bodyAsSource
        source.runWith(sink)
    }

    streamToFuture.andThen {
      case res => res.get
    } map(_ => {
      isInitialized = false
      Logger.info("Stream closed")
    })
  }
}
