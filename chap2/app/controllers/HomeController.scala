package controllers

import javax.inject._

import actors.TwitterStreamer
import akka.NotUsed
import akka.actor.{ActorRef, ActorSystem}
import akka.stream.{Materializer, OverflowStrategy}
import akka.stream.scaladsl.{Keep, Sink, Source}
import akka.util.ByteString
import org.reactivestreams.Publisher
import play.api.{Configuration, Logger}
import play.api.http.HttpEntity
import play.api.i18n.MessagesApi
import play.api.libs.oauth.{ConsumerKey, OAuthCalculator, RequestToken}
import play.api.libs.streams.ActorFlow
import play.api.libs.ws.WSClient
import play.api.mvc._

import scala.concurrent.ExecutionContext


@Singleton
class HomeController @Inject()(ws: WSClient,
                               config: Configuration,
                               cc: ControllerComponents,
                               messagesApi: MessagesApi
                              )
                              (implicit ec: ExecutionContext, system: ActorSystem, mat: Materializer) extends AbstractController(cc) {

  def index: Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    Logger.info("Action index")
    Ok(views.html.index())
  }


  def tweets: WebSocket = WebSocket.accept[String, String] { _: RequestHeader =>
    ActorFlow.actorRef { out =>
      Logger.info("Action tweets")
      val key: ConsumerKey = ConsumerKey(config.get[String]("twitter.apiKey"), config.get[String]("twitter.apiSecret"))
      val token: RequestToken = RequestToken(config.get[String]("twitter.token"), config.get[String]("twitter.tokenSecret"))
      TwitterStreamer.props(out, ws, OAuthCalculator(key, token))
    }
  }

  def replicatedFeed: Action[AnyContent] = Action { _: Request[AnyContent] =>
    Logger.warn("Replicate tweets")
    val (actorRef: ActorRef, publisher: Publisher[ByteString]) =
      Source.actorRef[ByteString](1000, OverflowStrategy.fail).toMat(Sink.asPublisher(false))(Keep.both).run()
    TwitterStreamer.replicate(actorRef)
    val source: Source[ByteString, NotUsed] = Source.fromPublisher(publisher)
    Result(header = ResponseHeader(200, Map.empty), body = HttpEntity.Streamed(source, None, None))
  }
}
