# README #

This code follows the book [Reactive Web Applications](https://www.manning.com/books/reactive-web-applications). It translates the code from the [original source code](https://github.com/manuelbernhardt/reactive-web-applications) to version 2.6.x of the Play framework.

## How to run the chapter 2 code? ##

### How do I get set up? ###

* Install sbt
* Enter your Twitter keys and tokens to conf/application.conf

### Run a single server instance ###

Open a terminal, change the working directory to chap2 and run:

    sbt run
    
The Twitter stream can be seen from multiple browser windows by visiting http://localhost:9000. 

### Replication ###
Make sure that a first instance runs on the default port 9000 and start sbt with

    sbt -DmasterNodeUrl=http://localhost:9000/replicate

Inside sbt, run a second instance on a different port:

    [twitter-stream] $ run 9001

The stream from the replicated node can be seen on http://localhost:9001

## Comments and pull requests are welcome ##
