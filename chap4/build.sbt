name := "simple-vocabulary-teacher"

version := "1.0"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.12.2"

libraryDependencies ++= Seq(
  guice
)

routesImport += "binders.PathBinders._"
routesImport += "binders.QueryStringBinders._"
