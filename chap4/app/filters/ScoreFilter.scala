package filters

import javax.inject.Inject

import akka.stream.Materializer
import akka.util.ByteString
import play.api.http.HttpEntity
import play.api.mvc.{Filter, RequestHeader, Result}

import scala.concurrent.{ExecutionContext, Future}

// See https://stackoverflow.com/questions/38947706/play-framework-how-to-modify-the-response-body-without-blocking?noredirect=1&lq=1
class ScoreFilter @Inject() (implicit val mat: Materializer, ec: ExecutionContext) extends Filter {
  override def apply(nextFilter: (RequestHeader) => Future[Result])
                    (rh: RequestHeader): Future[Result] = {

    val result: Future[Result] = nextFilter(rh)
    result.flatMap { res: Result =>
      if (res.header.status == 200 || res.header.status == 406) {
        val correct: Any = res.session(rh).get("correct").getOrElse(0)
        val wrong: Any = res.session(rh).get("wrong").getOrElse(0)
        val contentType: Option[String] = res.body.contentType
        val score: String = s"\n\nYour current score is: $correct correct answers and $wrong wrong answers"
        val scoreByteString: ByteString = ByteString(score.getBytes("UTF-8"))
        val maybeNewBody: Future[ByteString] = res.body.consumeData.map(_.concat(scoreByteString))
          maybeNewBody map { newBody: ByteString =>
            val x = HttpEntity.Strict(newBody, contentType)
            res.copy(body = x)
          }

      } else {
        Future.successful(res)
      }

    }
  }
}
