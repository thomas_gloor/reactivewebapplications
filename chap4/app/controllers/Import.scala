package controllers

import javax.inject.Inject

import models.Vocabulary
import play.api.i18n.Lang
import play.api.mvc.{AbstractController, ControllerComponents}
import services.VocabularyService

class Import @Inject()(cc: ControllerComponents, vocabulary: VocabularyService) extends AbstractController(cc) {
  def importWord(sourceLanguage: Lang,
                 word: String,
                 targetLanguage: Lang,
                 translation: String
                ) = Action { request =>
    val added = vocabulary.addVocabulary(Vocabulary(sourceLanguage, targetLanguage, word, translation))
    if (added)
      Ok
    else
      Conflict
  }
}
